package com.trendyol.booking.saga.handler;

import com.trendyol.booking.service.booking.BookingService;
import com.trendyol.common.constants.AmqpConstants;
import com.trendyol.common.converter.Converter;
import com.trendyol.common.saga.event.BookingCanceledEvent;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@AllArgsConstructor
public class BookingCanceledHandler {

	private final Converter converter;
	private final BookingService bookingService;

	@RabbitListener (queues = AmqpConstants.QUEUE_BOOKING_CANCELED)
	public void handleBookingCanceledEvent(@Payload String payload) {
		log.debug("Handling a cancelling booking event {}", payload);
		BookingCanceledEvent event = converter.toObject(payload, BookingCanceledEvent.class);
		bookingService.cancelBooking(event.getBookingDTO().getId());
	}
}
