package com.trendyol.booking.saga.handler;

import com.trendyol.booking.service.booking.BookingService;
import com.trendyol.common.constants.AmqpConstants;
import com.trendyol.common.converter.Converter;
import com.trendyol.common.saga.event.BookingCanceledEvent;
import com.trendyol.common.saga.event.BookingCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@AllArgsConstructor
public class BookingCreatedHandler {

	private final Converter converter;
	private final BookingService bookingService;

	@RabbitListener (queues = AmqpConstants.QUEUE_BOOKING_CREATED)
	public void handleBookingCreatedEvent(@Payload String payload) {
		log.info("Handling a booking created event {}", payload);
		BookingCreatedEvent event = converter.toObject(payload, BookingCreatedEvent.class);
		bookingService.createBooking(event.getBookingDTO());
	}
}
