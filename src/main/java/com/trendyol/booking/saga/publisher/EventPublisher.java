package com.trendyol.booking.saga.publisher;

import com.trendyol.common.constants.AmqpConstants;
import com.trendyol.common.converter.Converter;
import com.trendyol.common.saga.event.BookingCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@AllArgsConstructor
public class EventPublisher {

	private final Converter converter;
	private final RabbitTemplate rabbitTemplate;

	public void publishBookingCreatedEvent(BookingCreatedEvent event) {
		this.rabbitTemplate.convertAndSend(AmqpConstants.EXCHANGE_BOOKING_CREATED, Strings.EMPTY,converter.toJSON(event));
	}
}
