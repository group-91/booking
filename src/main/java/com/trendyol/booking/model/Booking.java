package com.trendyol.booking.model;

import com.trendyol.booking.constants.RepositoryConstants;
import com.trendyol.common.enums.BookingStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@Entity
@Table (name = "booking", schema = RepositoryConstants.SCHEME_BOOKING)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
public class Booking {

	@Id
	@Column (nullable = false, unique = true, updatable = false, length = 36)
	private String id;

	@Column (nullable = false, unique = false, updatable = false, length = 36)
	private String hotelId;

	@Column (nullable = false, unique = false, updatable = false, length = 36)
	private String roomId;

	@Column (nullable = false, unique = false, updatable = false, length = 36)
	private String customerId;

	@Column (nullable = false, unique = false, updatable = false, length = 36)
	private BookingStatus bookingStatus;

	@CreatedDate
	@Temporal (TIMESTAMP)
	private Date createdDate;

}
