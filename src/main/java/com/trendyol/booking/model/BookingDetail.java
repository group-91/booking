package com.trendyol.booking.model;

import com.trendyol.booking.constants.RepositoryConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table (name = "booking_detail", schema = RepositoryConstants.SCHEME_BOOKING)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
public class BookingDetail {

	@Id
	@Column (nullable = false, unique = true, updatable = false, length = 36)
	private String id;

	@Column (nullable = false)
	private BigDecimal totalPrice;

	@Column
	private Date startDate;

	@Column
	private Date endDate;

}
