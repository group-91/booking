package com.trendyol.booking.client.customer;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient (name = "customerClient", url = "localhost:8082/customers")
public interface CustomerFeignClient {

}
