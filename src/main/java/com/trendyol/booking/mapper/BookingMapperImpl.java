package com.trendyol.booking.mapper;

import com.trendyol.booking.model.Booking;
import com.trendyol.common.model.BookingDTO;
import org.springframework.stereotype.Component;

@Component
public class BookingMapperImpl implements BookingMapper{

	@Override
	public BookingDTO toDTO(Booking entity) {
		return BookingDTO.builder()
				.id(entity.getId())
				.hotelId(entity.getHotelId())
				.roomId(entity.getRoomId())
				.customerId(entity.getCustomerId())
				.bookingStatus(entity.getBookingStatus())
				.build();
	}

	@Override
	public Booking toEntity(BookingDTO dto) {
		return Booking.builder()
				.id(dto.getId())
				.hotelId(dto.getHotelId())
				.roomId(dto.getRoomId())
				.customerId(dto.getCustomerId())
				.bookingStatus(dto.getBookingStatus())
				.build();
	}
}
